package com.trueberry.htremote.callbacks;

import android.os.AsyncTask;

import com.trueberry.htremote.db.FileInformation;

import static com.trueberry.htremote.MainActivity.db;

public class SavedPositionsDeleteTask extends AsyncTask<String, Void, Object> {

    @Override
    protected Object doInBackground(String... objects) {
        try {
            String name = objects[0];
            FileInformation dbRecord = db.fileInformationDao().findByName(name);
            if (dbRecord != null) {
                db.fileInformationDao().delete(dbRecord);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}