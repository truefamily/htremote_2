package com.trueberry.htremote.callbacks;

import android.os.AsyncTask;

import com.eeeeeric.mpc.hc.api.MediaPlayerClassicHomeCinema;
import com.eeeeeric.mpc.hc.api.TimeCode;
import com.eeeeeric.mpc.hc.api.WMCommand;
import com.trueberry.htremote.ClientConnection;
import com.trueberry.htremote.MainActivity;
import com.trueberry.htremote.MainFragment;
import com.trueberry.htremote.MyApp;

import java.io.IOException;
import java.util.Map;

import okhttp3.Request;

public class MPCAsyncTask extends AsyncTask<Object, Void, Object> {

    private static String ip = null;
    private static Integer port = null;

    private TaskDelegate delegate;

    public MPCAsyncTask(TaskDelegate delegate) {
        this.delegate = delegate;
    }

    @Override
    protected Object doInBackground(Object... objects) {
        try {
            if (ip == null || port == null) {
                ip = MainActivity.PREFS.getString("settings_ip", null);
                String portStr = MainActivity.PREFS.getString("settings_mpc_port", null);
                if (ip == null || portStr == null) {
                    throw new RuntimeException("IP or MPC-PORT is not set");
                }
                port = Integer.valueOf(portStr);
            }
            MediaPlayerClassicHomeCinema mpc = new MediaPlayerClassicHomeCinema(ip, port);
            switch ((MPCCommand) objects[0]) {
                case FULLSCREEN:
                    mpc.toggleFullscreen();
                    break;
                case GET_DURATION:
                    return mpc.getDuration();
                case GET_POSITION:
                    return mpc.getPosition();
                case GET_VOLUME:
                    return mpc.getVolume();
                case GET_INFO:
                    return mpc.getVariables();
                case PLAYPAUSE:
                    mpc.togglePlayPause();
                    break;
                case PAUSE:
                    mpc.pause();
                    break;
                case REWIND:
                    TimeCode timeCode = new TimeCode((Integer) objects[1]);
                    mpc.seek(timeCode);
                    break;
                case SET_MUTE:
                    mpc.toggleMute();
                    break;
                case SET_VOLUME:
                    mpc.setVolume((int) objects[1]);
                    break;
                case STOP:
                    mpc.stop();
                    break;
                case CLOSE:
                    mpc.execute(WMCommand.EXIT);
                    break;
            }
        } catch (Exception e) {
            return e;
        }
        return null;
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
        if (delegate != null) {
            delegate.taskComplete(o);
        }
    }

    private void tryToDeleteFile() throws IOException {

    }
}
