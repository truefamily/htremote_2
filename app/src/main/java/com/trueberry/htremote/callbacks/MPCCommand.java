package com.trueberry.htremote.callbacks;

public enum MPCCommand {
    FULLSCREEN,
    GET_DURATION,
    GET_POSITION,
    GET_VOLUME,
    GET_INFO,
    PLAYPAUSE,
    PAUSE,
    SET_MUTE,
    SET_VOLUME,
    STOP,
    CLOSE,
    REWIND
}
