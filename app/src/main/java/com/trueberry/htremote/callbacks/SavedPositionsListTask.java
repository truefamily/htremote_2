package com.trueberry.htremote.callbacks;

import android.os.AsyncTask;
import android.view.View;
import android.widget.ListView;

import com.trueberry.htremote.db.FileInformation;
import com.trueberry.htremote.list.FilePositionListAdapter;

import java.util.List;

import static com.trueberry.htremote.MainActivity.db;

public class SavedPositionsListTask extends AsyncTask<Object, Void, Object> {

    @Override
    protected Object doInBackground(Object... objects) {
        try {
            List<FileInformation> items = db.fileInformationDao().getAll();
            View view = (View) objects[0];
            ListView listView = (ListView) objects[1];
            if (view != null && listView != null) {
                final FilePositionListAdapter adapter = new FilePositionListAdapter(view.getContext(), items);
                view.post(() -> {
                    listView.setAdapter(adapter);
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
