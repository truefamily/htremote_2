package com.trueberry.htremote.callbacks;

public interface TaskDelegate {
    void taskComplete(Object result);
}