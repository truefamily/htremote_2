package com.trueberry.htremote.callbacks;

import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.trueberry.htremote.R;
import com.trueberry.htremote.list.FileListAdapter;
import com.trueberry.htremote.list.FileItem;

import org.apache.commons.codec.binary.Hex;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class ListCallback implements Callback {

    private ListView itemsList;
    private View v;
    private ProgressBar progressBar;

    public ListCallback(ListView itemsList, View v) {
        this.itemsList = itemsList;
        this.v = v;
        this.progressBar = v.findViewById(R.id.progressbar);
    }

    @Override
    public void onFailure(Call call, final IOException e) {
        v.post(() -> {
            progressBar.setVisibility(View.INVISIBLE);
            Toast.makeText(v.getContext(), e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
        });
    }

    @Override
    public void onResponse(Call call, Response response) throws IOException {
        progressBar = v.findViewById(R.id.progressbar);
        try (ResponseBody responseBody = response.body()) {
            if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
            final List<FileItem> files = new ArrayList<>();
            ByteArrayInputStream in = new ByteArrayInputStream(Hex.decodeHex(responseBody.string().toCharArray()));
            Object[] fileObjects = (Object[]) new ObjectInputStream(in).readObject();
            for (Object fileObject : fileObjects) {
                TypeReference<HashMap<String, String>> typeRef = new TypeReference<HashMap<String, String>>() {};
                ObjectMapper mapper = new ObjectMapper();
                Map<String, String> map = mapper.readValue((String) fileObject, typeRef);
                files.add(new FileItem(map.get("name"), map.get("path"), Boolean.parseBoolean(map.get("isDirectory"))));
            }
            final boolean hasFiles = files.size() > 0;
            final FileListAdapter adapter = new FileListAdapter(v.getContext(), files);
            v.post(() -> {
                progressBar.setVisibility(View.INVISIBLE);
                if (hasFiles) {
                    itemsList.setAdapter(adapter);
                } else {
                    Toast.makeText(v.getContext(), R.string.empty_dir, Toast.LENGTH_SHORT).show();
                }
            });
        } catch (final Exception e) {
            v.post(() -> {
                progressBar.setVisibility(View.INVISIBLE);
                Toast.makeText(v.getContext(), e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            });
        }
    }
}
