package com.trueberry.htremote.callbacks;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.support.design.widget.BottomNavigationView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.trueberry.htremote.ControlFragment;
import com.trueberry.htremote.R;
import com.trueberry.htremote.list.FileItem;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Headers;
import okhttp3.Response;

public class ExecCallback implements Callback {

    private View v;
    private Activity activity;
    private ProgressBar progressBar;

    public ExecCallback(View v, Activity activity) {
        this.v = v;
        this.activity = activity;
        this.progressBar = v.findViewById(R.id.progressbar);
    }

    @Override
    public void onFailure(Call call, final IOException e) {
        v.post(() -> {
            if (progressBar != null)  {
                progressBar.setVisibility(View.INVISIBLE);
            }
            Toast.makeText(v.getContext(), e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
        });
    }

    @Override
    public void onResponse(Call call, Response response) throws IOException {
        v.post(() -> {
            if (progressBar != null)  {
                progressBar.setVisibility(View.INVISIBLE);
            }
        });
        switchFragment();
    }

    private void switchFragment() {
        v.post(() -> {
            BottomNavigationView navigation = activity.findViewById(R.id.navigation);
            navigation.setSelectedItemId(R.id.navigation_control);
        });
    }
}
