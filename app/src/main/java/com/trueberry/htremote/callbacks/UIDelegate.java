package com.trueberry.htremote.callbacks;

public interface UIDelegate {
    void fail(Exception e);
    void complete();
}