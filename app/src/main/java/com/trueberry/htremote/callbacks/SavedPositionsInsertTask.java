package com.trueberry.htremote.callbacks;

import android.os.AsyncTask;

import com.trueberry.htremote.db.FileInformation;

import static com.trueberry.htremote.MainActivity.db;

public class SavedPositionsInsertTask extends AsyncTask<Object, Void, Object> {

    @Override
    protected Object doInBackground(Object... objects) {
        try {
            FileInformation information = (FileInformation) objects[0];
            FileInformation dbRecord = db.fileInformationDao().findByName(information.getName());
            if (dbRecord != null) {
                dbRecord.setTimeCode(information.getTimeCode());
                db.fileInformationDao().update(dbRecord);
            } else {
                db.fileInformationDao().insertAll(information);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}