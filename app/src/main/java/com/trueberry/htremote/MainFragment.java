package com.trueberry.htremote;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.trueberry.htremote.callbacks.ExecCallback;
import com.trueberry.htremote.callbacks.ListCallback;
import com.trueberry.htremote.list.FileItem;

import org.apache.commons.codec.binary.Hex;

import okhttp3.OkHttpClient;
import okhttp3.Request;

public class MainFragment extends Fragment {

    public static final OkHttpClient client = new OkHttpClient();

    private ListView itemsList;

    public MainFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_main, container, false);
        itemsList = view.findViewById(R.id.itemsList);
        final ProgressBar progressBar = view.findViewById(R.id.progressbar);

        Button connectToServerBtn = view.findViewById(R.id.connect_btn);
        connectToServerBtn.setOnClickListener(v -> {
            try {
                progressBar.setVisibility(View.VISIBLE);
                Request request = new Request.Builder().url(ClientConnection.getListUri()).build();
                client.newCall(request).enqueue(new ListCallback(itemsList, view));
                view.findViewById(R.id.back_btn).setEnabled(true);
            } catch (Exception e) {
                Toast.makeText(v.getContext(), e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            } finally {
                progressBar.setVisibility(View.INVISIBLE);
            }
        });

        Button upBtn = view.findViewById(R.id.back_btn);
        upBtn.setOnClickListener(v -> {
            progressBar.setVisibility(View.VISIBLE);
            Request request = new Request.Builder().url(ClientConnection.getListUri()).addHeader("parent", "true").build();
            client.newCall(request).enqueue(new ListCallback(itemsList, view));
        });

        itemsList.setOnItemClickListener((parent, v, position, id) -> {
            progressBar.setVisibility(View.VISIBLE);
            Request.Builder builder = new Request.Builder().url(ClientConnection.getListUri());
            final FileItem item = (FileItem) parent.getItemAtPosition(position);
            String encodedPath = new String(Hex.encodeHex(item.getPath().getBytes()));
            if (item.isDirectory()) {
                builder.addHeader("path", encodedPath);
                client.newCall(builder.build()).enqueue(new ListCallback(itemsList, view));
            } else {
                Request request = new Request.Builder()
                        .url(ClientConnection.getExecUri())
                        .addHeader("path", encodedPath)
                        .build();
                client.newCall(request).enqueue(new ExecCallback(view, getActivity()));
            }
        });

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (!(context instanceof OnFragmentInteractionListener)) {
            throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
