package com.trueberry.htremote.list;

/**
 * Информация о файле
 */
public class FileItem {

    private String name;
    private String path;
    private boolean isDirectory;

    public FileItem(String name, String path, boolean isDirectory) {
        this.name = name;
        this.path = path;
        this.isDirectory = isDirectory;
    }

    public String getName() {
        return name;
    }

    public String getPath() {
        return path;
    }

    public boolean isDirectory() {
        return isDirectory;
    }
}
