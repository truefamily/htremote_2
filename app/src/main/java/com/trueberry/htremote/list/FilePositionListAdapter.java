package com.trueberry.htremote.list;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.trueberry.htremote.R;
import com.trueberry.htremote.db.FileInformation;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class FilePositionListAdapter extends BaseAdapter {

    private static SimpleDateFormat sdf = new SimpleDateFormat("HH:MM dd MMMM yyyy");

    private LayoutInflater lInflater;
    private List<FileInformation> files;

    public FilePositionListAdapter(Context context, List<FileInformation> products) {
        files = products;
        lInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return files.size();
    }

    @Override
    public FileInformation getItem(int position) {
        return files.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = lInflater.inflate(R.layout.saved_pos_list_item, parent, false);
        }
        FileInformation item = getItem(position);
        ((TextView) view.findViewById(R.id.saved_pos_name)).setText(item.getName());
        ((TextView) view.findViewById(R.id.saved_pos_position)).setText("Position: " + item.getTimeCode());
        ((TextView) view.findViewById(R.id.saved_pos_timeAdded)).setText("added at " + sdf.format(new Date(item.getAddedTime())));
        return view;
    }
}
