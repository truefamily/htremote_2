package com.trueberry.htremote.list;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.trueberry.htremote.R;

import java.util.Comparator;
import java.util.List;

public class FileListAdapter extends BaseAdapter {

    private Context ctx;
    private LayoutInflater lInflater;
    private List<FileItem> files;

    public FileListAdapter(Context context, List<FileItem> products) {
        ctx = context;
        files = products;
        files.sort(Comparator.comparing(fileItem -> !fileItem.isDirectory()));
        lInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return files.size();
    }

    @Override
    public FileItem getItem(int position) {
        return files.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = lInflater.inflate(R.layout.file_list_item, parent, false);
        }
        FileItem item = getItem(position);
        ((TextView) view.findViewById(R.id.itemName)).setText(item.getName());
        ImageView isDir = view.findViewById(R.id.itemDirImg);
        isDir.setVisibility(item.isDirectory() ? View.VISIBLE : View.INVISIBLE);
        return view;
    }
}
