package com.trueberry.htremote;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.trueberry.htremote.callbacks.ExecCallback;
import com.trueberry.htremote.callbacks.SavedPositionsDeleteTask;
import com.trueberry.htremote.callbacks.SavedPositionsListTask;
import com.trueberry.htremote.db.FileInformation;

import org.apache.commons.codec.binary.Hex;

import okhttp3.Request;

public class SavedPositionsFragment extends Fragment {

    public SavedPositionsFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_saved_positions, container, false);
        ListView itemsList = view.findViewById(R.id.saved_pos_list_view);
        itemsList.setOnItemClickListener((parent, v, position, id) -> {
            final FileInformation item = (FileInformation) parent.getItemAtPosition(position);
            String encodedPath = new String(Hex.encodeHex(item.getPath().getBytes()));
            Request request = new Request.Builder().url(ClientConnection.getExecUri())
                    .addHeader("path", encodedPath)
                    .addHeader("name", item.getName())
                    .addHeader("position", item.getTimeCode())
                    .build();
            MainFragment.client.newCall(request).enqueue(new ExecCallback(view, getActivity()));
        });
        itemsList.setOnItemLongClickListener((parent, view1, position, id) -> {
            final FileInformation item = (FileInformation) parent.getItemAtPosition(position);
            new AlertDialog.Builder(SavedPositionsFragment.this.getContext())
                    .setTitle(R.string.delete_record)
                    .setPositiveButton(R.string.yes, (dialog, which) -> {
                        new SavedPositionsDeleteTask().execute(item.getName());
                        BottomNavigationView navigation = getActivity().findViewById(R.id.navigation);
                        navigation.setSelectedItemId(R.id.navigation_home);
                        Toast.makeText(getContext(), R.string.record_deleted, Toast.LENGTH_SHORT).show();
                    })
                    .setNegativeButton(R.string.no, (dialog, which) -> {
                        Toast.makeText(getContext(), R.string.delete_canceled, Toast.LENGTH_SHORT).show();
                    })
                    .setMessage(SavedPositionsFragment.this.getString(R.string.delete_saved_record, item.getName()))
                    .show();
            return true;
        });

        new SavedPositionsListTask().execute(view, itemsList);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (!(context instanceof OnFragmentInteractionListener)) {
            throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
/*        BottomNavigationView navigation = getActivity().findViewById(R.id.navigation);
        navigation.setSelectedItemId(R.id.navigation_home);*/
    }
}
