package com.trueberry.htremote;

import android.app.Application;

import com.eeeeeric.mpc.hc.api.TimeCode;
import com.eeeeeric.mpc.hc.api.TimeCodeException;
import com.trueberry.htremote.callbacks.MPCCommand;
import com.trueberry.htremote.callbacks.UIDelegate;

import org.apache.commons.codec.binary.Hex;

public class MyApp extends Application {

    private static String filePath;
    private static String duration;
    private static String position;
    private static String volumelevel;

    public static String getFilePath() {
        return filePath;
    }

    public static String getEncodedFilePath() {
        return new String(Hex.encodeHex(MyApp.filePath.getBytes()));
    }

    public static void setFilePath(String filePath) {
        MyApp.filePath = filePath;
    }

    public static String getFileName() {
        return MyApp.filePath == null ? null : MyApp.filePath.substring(MyApp.filePath.lastIndexOf("\\") + 1);
    }

    public static String getDuration() {
        return duration;
    }

    public static String getDurationTextView() {
        return "Dur: " + duration;
    }

    public static int getDurationTotalSeconds() throws TimeCodeException {
        return duration == null ? 0 : new TimeCode(duration).getTotalSeconds();
    }

    public static void setDuration(String duration) {
        MyApp.duration = duration;
    }

    public static String getPosition() {
        return position;
    }

    public static String getPositionTextView() {
        return "Pos: " + position;
    }

    public static int getPositionTotalSeconds() throws TimeCodeException {
        return position == null ? 0 : new TimeCode(position).getTotalSeconds();
    }

    public static void setPosition(String position) {
        MyApp.position = position;
    }

    public static String getVolumelevel() {
        return volumelevel;
    }

    public static Integer getIntVolumelevel() {
        return volumelevel == null ? 0 : Integer.valueOf(volumelevel);
    }

    public static void setVolumelevel(String volumelevel) {
        MyApp.volumelevel = volumelevel;
    }

    public static void refreshFileAttrs(UIDelegate uiDelegate) {
        ClientConnection.getInstance().tryToExecute(null, uiDelegate, MPCCommand.GET_INFO);
    }
}
