package com.trueberry.htremote.db;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import java.util.Date;

@Entity
public class FileInformation {

    public FileInformation(String path, String name, String timeCode, long addedTime) {
        this.path = path;
        this.name = name;
        this.timeCode = timeCode;
        this.addedTime = addedTime;
    }

    @PrimaryKey(autoGenerate = true)
    private int uid;

    public void setUid(int uid) {
        this.uid = uid;
    }

    @ColumnInfo
    private String path;

    @ColumnInfo
    private String name;

    @ColumnInfo
    private String timeCode;

    @ColumnInfo
    private long addedTime;

    public int getUid() {
        return uid;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTimeCode() {
        return timeCode;
    }

    public void setTimeCode(String timeCode) {
        this.timeCode = timeCode;
    }

    public long getAddedTime() {
        return addedTime;
    }

    public void setAddedTime(long addedTime) {
        this.addedTime = addedTime;
    }
}
