package com.trueberry.htremote.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

@Dao
public interface FileInformationDao {

    @Query("SELECT * FROM fileinformation")
    List<FileInformation> getAll();

    @Query("SELECT * FROM fileinformation WHERE name LIKE :name LIMIT 1")
    FileInformation findByName(String name);

    @Query("SELECT * FROM fileinformation WHERE path LIKE :path LIMIT 1")
    FileInformation findByPath(String path);

    @Insert(onConflict = REPLACE)
    void insertAll(FileInformation... fileInformations);

    @Update(onConflict = REPLACE)
    void update(FileInformation fileInformation);

    @Delete
    void delete(FileInformation fileInformation);
}