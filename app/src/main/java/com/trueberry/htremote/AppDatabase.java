package com.trueberry.htremote;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.trueberry.htremote.db.FileInformationDao;
import com.trueberry.htremote.db.FileInformation;

@Database(entities = {FileInformation.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract FileInformationDao fileInformationDao();
}