package com.trueberry.htremote;

import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.view.View;

import com.trueberry.htremote.callbacks.MPCCommand;

import static com.trueberry.htremote.MainActivity.PREFS;

public class CallStateListener extends PhoneStateListener {

    private View view;

    CallStateListener(View view) {
        this.view = view;
    }

    @Override
    public void onCallStateChanged(int state, String incomingNumber) {
        if (PREFS.getBoolean("settings_pause", false) && state == TelephonyManager.CALL_STATE_RINGING) {
            ClientConnection.getInstance().tryToExecute(view.getContext(), MPCCommand.PAUSE);
        }
    }
}
