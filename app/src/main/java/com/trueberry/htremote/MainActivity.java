package com.trueberry.htremote;

import android.app.Activity;
import android.app.Fragment;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.preference.PreferenceManager;
import android.support.design.widget.BottomNavigationView;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;

public class MainActivity extends Activity implements OnFragmentInteractionListener {

    public static SharedPreferences PREFS;
    public static AppDatabase db = null;
    BottomNavigationView navigation;
    private int saveState;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener = item -> {
                switch (item.getItemId()) {
                    case R.id.navigation_home:
                        switchFragment(new MainFragment());
                        return true;
                    case R.id.navigation_saved:
                        switchFragment(new SavedPositionsFragment());
                        return true;
                    case R.id.navigation_control:
                        switchFragment(new ControlFragment());
                        return true;
                    case R.id.navigation_settings:
                        switchFragment(new SettingsFragment());
                        return true;
                }
                return false;
            };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PREFS = PreferenceManager.getDefaultSharedPreferences(this);
        db = Room.databaseBuilder(getApplicationContext(), AppDatabase.class, "htremote_db").build();
        setContentView(R.layout.activity_app);

        navigation = findViewById(R.id.navigation);
        BottomNavigationViewHelper.disableShiftMode(navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        TelephonyManager tm = (TelephonyManager) getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE);
        assert tm != null;
        tm.listen(new CallStateListener(findViewById(android.R.id.content)), PhoneStateListener.LISTEN_CALL_STATE);

        if(savedInstanceState != null){
            navigation.setSelectedItemId(saveState);
        } else {
            navigation.setSelectedItemId(R.id.navigation_home);
        }
    }

    public void switchFragment(Fragment fragment) {
        getFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .addToBackStack(fragment.getTag())
                .commit();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        navigation.setSelectedItemId(saveState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        saveState = navigation.getSelectedItemId();
    }

}
