package com.trueberry.htremote;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.trueberry.htremote.callbacks.MPCAsyncTask;
import com.trueberry.htremote.callbacks.TaskDelegate;
import com.trueberry.htremote.callbacks.UIDelegate;

import java.util.Map;

public class ClientConnection implements TaskDelegate {

    private static ClientConnection connection = new ClientConnection();

    private UIDelegate uiDelegate = null;

    public static String getListUri() {
        return getRoot() + "list";
    }

    public static String getExecUri() {
        return getRoot() + "exec";
    }

    public static String getDeleteFileUri() {
        return getRoot() + "delete";
    }

    private static String getRoot() {
        String ip = MainActivity.PREFS.getString("settings_ip", null);
        String port = MainActivity.PREFS.getString("settings_port", null);
        if (ip == null || port == null) {
            throw new RuntimeException("Не заданы настройки IP и PORT сервера");
        }
        return "http://" + ip + ":" + port + "/";
    }

    private static AsyncTask currentTask;
    private static Toast toast;

    public static ClientConnection getInstance() {
        return connection;
    }

    public AsyncTask tryToExecute(Context context, Object... params) {
        return tryToExecute(context, null, params);
    }

    public AsyncTask tryToExecute(Context context, UIDelegate uiDelegate, Object... params) {
        if (currentTask == null || currentTask.getStatus() == AsyncTask.Status.FINISHED) {
            this.uiDelegate = uiDelegate;
            TaskDelegate taskDelegate = uiDelegate != null ? this : null;
            currentTask = new MPCAsyncTask(taskDelegate).execute(params);
            return currentTask;
        }
        if (context != null) {
            if (toast != null) {
                toast.cancel();
            }
            toast = Toast.makeText(context, "Выполняется...", Toast.LENGTH_SHORT);
            toast.show();
        }
        return null;
    }

    @Override
    public void taskComplete(Object result) {
        if (result instanceof Exception) {
            uiDelegate.fail((Exception) result);
            return;
        }
        if (result != null) {
            Map map = (Map) result;
            MyApp.setVolumelevel((String) map.get("volumelevel"));
            MyApp.setFilePath((String) map.get("filepath"));
            MyApp.setDuration((String)map.get("durationstring"));
            MyApp.setPosition((String) map.get("positionstring"));
        }
        if (uiDelegate != null) {
            uiDelegate.complete();
        }
    }
}
