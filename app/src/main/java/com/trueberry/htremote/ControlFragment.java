package com.trueberry.htremote;

import android.app.Fragment;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.trueberry.htremote.callbacks.MPCCommand;
import com.trueberry.htremote.callbacks.UIDelegate;
import com.trueberry.htremote.dialogs.DeleteDialog;
import com.trueberry.htremote.dialogs.RewindDialog;
import com.trueberry.htremote.dialogs.SavePosDialog;

public class ControlFragment extends Fragment {

    private Button refreshBtn, rewindBtn, muteBtn, closeBtn, deleteBtn;
    private ImageButton playPauseBtn, fullscreenBtn;
    private SeekBar volumeBar;
    private TextView fileNameTv, filePosTv, fileDurTv;

    private OnFragmentInteractionListener mListener;

    public ControlFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_control, container, false);

        fileNameTv = view.findViewById(R.id.fileName);
        filePosTv = view.findViewById(R.id.filePosition);
        fileDurTv = view.findViewById(R.id.fileDuration);

        refreshBtn = view.findViewById(R.id.refresh_btn);
        refreshBtn.setOnClickListener(v -> {
            MyApp.refreshFileAttrs(new UIDelegate() {
                @Override
                public void fail(Exception e) {
                    Toast.makeText(getContext(), R.string.error_try_again, Toast.LENGTH_LONG).show();
                }
                @Override
                public void complete() {
                    fileNameTv.setText(MyApp.getFileName());
                    filePosTv.setText(MyApp.getPositionTextView());
                    fileDurTv.setText(MyApp.getDurationTextView());
                    volumeBar.setProgress(MyApp.getIntVolumelevel());
                }
            });
        });
        muteBtn = view.findViewById(R.id.mute_btn);
        muteBtn.setOnClickListener(v -> {
            ClientConnection.getInstance().tryToExecute(view.getContext(), MPCCommand.SET_MUTE);
        });
        rewindBtn = view.findViewById(R.id.rewind_btn);
        rewindBtn.setOnClickListener(v -> {
            MyApp.refreshFileAttrs(new UIDelegate() {
                @Override
                public void fail(Exception e) {
                    Toast.makeText(getContext(), R.string.error_try_again, Toast.LENGTH_LONG).show();
                }
                @Override
                public void complete() {
                    RewindDialog rewindDialog = new RewindDialog();
                    rewindDialog.show(getFragmentManager(), "RewindDialog");
                }
            });
        });
        playPauseBtn = view.findViewById(R.id.play_btn);
        playPauseBtn.setOnClickListener(v -> {
            ClientConnection.getInstance().tryToExecute(view.getContext(), MPCCommand.PLAYPAUSE);
        });
        fullscreenBtn = view.findViewById(R.id.fullscreen_btn);
        fullscreenBtn.setOnClickListener(v -> {
            ClientConnection.getInstance().tryToExecute(view.getContext(), MPCCommand.FULLSCREEN);
        });
        closeBtn = view.findViewById(R.id.close_btn);
        closeBtn.setOnClickListener(v -> {
            MyApp.refreshFileAttrs(new UIDelegate() {
                @Override
                public void fail(Exception e) {
                    Toast.makeText(getContext(), R.string.error_try_again, Toast.LENGTH_LONG).show();
                }
                @Override
                public void complete() {
                    SavePosDialog saveDialog = new SavePosDialog();
                    saveDialog.show(getFragmentManager(), "SaveDialog");
                }
            });
        });
        deleteBtn = view.findViewById(R.id.delete_btn);
        deleteBtn.setOnClickListener(v -> {
            MyApp.refreshFileAttrs(new UIDelegate() {
                @Override
                public void fail(Exception e) {
                    Toast.makeText(getContext(), R.string.error_try_again, Toast.LENGTH_LONG).show();
                }
                @Override
                public void complete() {
                    DeleteDialog saveDialog = new DeleteDialog();
                    saveDialog.show(getFragmentManager(), "DeleteDialog");
                }
            });
        });
        volumeBar = view.findViewById(R.id.volumeBar);
        volumeBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) { }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) { }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                ClientConnection.getInstance().tryToExecute(view.getContext(), MPCCommand.SET_VOLUME, seekBar.getProgress());
            }
        });
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
}
