package com.trueberry.htremote.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

import com.eeeeeric.mpc.hc.api.TimeCode;
import com.eeeeeric.mpc.hc.api.TimeCodeException;
import com.trueberry.htremote.ClientConnection;
import com.trueberry.htremote.MyApp;
import com.trueberry.htremote.R;
import com.trueberry.htremote.callbacks.MPCCommand;

public class RewindDialog extends DialogFragment implements DialogInterface.OnClickListener {

    final String LOG_TAG = "RewindDialog";

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            LayoutInflater inflater = getActivity().getLayoutInflater();
            View view = inflater.inflate(R.layout.rewind_dialog, null);
            TextView rewindDur = view.findViewById(R.id.rewindDur);
            rewindDur.setText(MyApp.getDuration());
            TextView rewindPos = view.findViewById(R.id.rewindPos);
            rewindPos.setText(MyApp.getPosition());
            TextView rewindNewPos = view.findViewById(R.id.rewindNewPos);
            rewindNewPos.setText(MyApp.getPosition());
            SeekBar rewindSeekBar = view.findViewById(R.id.rewindSeekBar);
            rewindSeekBar.setMax(MyApp.getDurationTotalSeconds());
            rewindSeekBar.setProgress(MyApp.getPositionTotalSeconds());
            rewindSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) { }
                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {}
                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                    try {
                        rewindNewPos.setText(new TimeCode(seekBar.getProgress()).toString());
                    } catch (TimeCodeException e) {
                        e.printStackTrace();
                    }
                }
            });
            builder.setView(view)
                    .setPositiveButton(R.string.rewind, (dialog, id) -> ClientConnection.getInstance().tryToExecute(getContext(), MPCCommand.REWIND, rewindSeekBar.getProgress()))
                    .setNegativeButton(R.string.cancel, (dialog, id) -> RewindDialog.this.getDialog().cancel());
            return builder.create();
        } catch (TimeCodeException e) {
            Log.e(LOG_TAG, getString(R.string.dialog_error), e);
            throw new RuntimeException(getString(R.string.dialog_error));
        }
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
    }
}

