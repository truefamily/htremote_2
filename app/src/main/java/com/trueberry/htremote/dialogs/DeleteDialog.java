package com.trueberry.htremote.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.trueberry.htremote.ClientConnection;
import com.trueberry.htremote.MainFragment;
import com.trueberry.htremote.MyApp;
import com.trueberry.htremote.R;
import com.trueberry.htremote.callbacks.MPCCommand;
import com.trueberry.htremote.callbacks.UIDelegate;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Request;
import okhttp3.Response;

public class DeleteDialog extends DialogFragment implements DialogInterface.OnClickListener {

    final String LOG_TAG = "DeleteDialog";

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder adb = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.delete_dialog_title)
                .setPositiveButton(R.string.delete, this)
                .setNegativeButton(R.string.cancel, this)
                .setMessage(getString(R.string.delete_message, MyApp.getFilePath()));
        return adb.create();
    }

    public void onClick(DialogInterface dialog, int which) {
        try {
            Context context = getContext();
            switch (which) {
                case Dialog.BUTTON_POSITIVE:
                    UIDelegate delegate = new UIDelegate() {
                        @Override
                        public void fail(Exception e) {
                            Toast.makeText(context.getApplicationContext(), R.string.error_try_again, Toast.LENGTH_LONG).show();
                        }
                        @Override
                        public void complete() {
                            if (MyApp.getFilePath() != null) {
                                Request request = new Request.Builder()
                                        .url(ClientConnection.getDeleteFileUri())
                                        .addHeader("path", MyApp.getEncodedFilePath())
                                        .build();
                                MainFragment.client.newCall(request).enqueue(new Callback() {
                                    @Override
                                    public void onFailure(Call call, IOException e) {
                                        Toast.makeText(context.getApplicationContext(), R.string.error_try_again, Toast.LENGTH_LONG).show();
                                    }

                                    @Override
                                    public void onResponse(Call call, Response response) throws IOException {
                                        Toast.makeText(context.getApplicationContext(), R.string.file_deleted, Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                        }
                    };
                    ClientConnection.getInstance().tryToExecute(getContext(), delegate, MPCCommand.CLOSE);

                    break;
                case Dialog.BUTTON_NEGATIVE:
                    break;
            }
        } catch (Exception e) {
            Log.e(LOG_TAG, getString(R.string.dialog_error), e);
        }
    }

    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
    }

    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
    }
}

