package com.trueberry.htremote.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.trueberry.htremote.ClientConnection;
import com.trueberry.htremote.MyApp;
import com.trueberry.htremote.R;
import com.trueberry.htremote.callbacks.MPCCommand;
import com.trueberry.htremote.callbacks.SavedPositionsInsertTask;
import com.trueberry.htremote.db.FileInformation;

import java.util.Date;

public class SavePosDialog extends DialogFragment implements DialogInterface.OnClickListener {

    final String LOG_TAG = "SaveDialog";

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder adb = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.save_dislog_title)
                .setPositiveButton(R.string.save, this)
                .setNegativeButton(R.string.not_save, this)
                .setMessage(getString(R.string.save_msg, MyApp.getFileName(), MyApp.getPosition()));
        return adb.create();
    }

    public void onClick(DialogInterface dialog, int which) {
        try {
            switch (which) {
                case Dialog.BUTTON_POSITIVE:
                    FileInformation information = new FileInformation(
                            MyApp.getFilePath(),
                            MyApp.getFileName(),
                            MyApp.getPosition(),
                            new Date().getTime()
                    );
                    new SavedPositionsInsertTask().execute(information);
                    ClientConnection.getInstance().tryToExecute(getContext(), MPCCommand.CLOSE);
                    getActivity().runOnUiThread(() -> {
                        Toast.makeText(getContext(), R.string.save_cur_pos, Toast.LENGTH_LONG).show();
                    });
                    break;
                case Dialog.BUTTON_NEGATIVE:
                    ClientConnection.getInstance().tryToExecute(getContext(), MPCCommand.CLOSE);
                    getActivity().runOnUiThread(() -> {
                        Toast.makeText(getContext(), R.string.mpc_closing, Toast.LENGTH_LONG).show();
                    });
                    break;
            }
        } catch (Exception e) {
            Log.e(LOG_TAG, getString(R.string.dialog_error), e);
        }
    }

    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
    }

    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
    }
}

